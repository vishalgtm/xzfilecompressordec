package com.company;

import org.apache.commons.compress.compressors.xz.XZCompressorInputStream;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class uncompress {
    public static void main(String[] args) {
        try {
            int buffersize=1024;
            InputStream fin = Files.newInputStream(Paths.get("dicomfile.tar.xz"));
            BufferedInputStream in = new BufferedInputStream(fin);
            OutputStream out = Files.newOutputStream(Paths.get("dicomfile.dcm"));
            XZCompressorInputStream xzIn = new XZCompressorInputStream(in);
            final byte[] buffer = new byte[buffersize];
            int n = 0;
            while (-1 != (n = xzIn.read(buffer))) {
                out.write(buffer, 0, n);
            }
            out.close();
            xzIn.close();
        }catch (Exception e){
            System.out.println(e);
        }
    }
}
