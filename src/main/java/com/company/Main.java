package com.company;


import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.compressors.xz.XZCompressorOutputStream;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) {
try {
    int buffersize=1024;
    InputStream in = Files.newInputStream(Paths.get("/root/Desktop/dicomfile.dcm"));
    OutputStream fout = Files.newOutputStream(Paths.get("dicomfile.tar.xz"));
    BufferedOutputStream out = new BufferedOutputStream(fout);
    XZCompressorOutputStream xzOut = new XZCompressorOutputStream(out);
    final byte[] buffer = new byte[buffersize];
    int n = 0;
    while (-1 != (n = in.read(buffer))) {
        xzOut.write(buffer, 0, n);
    }
    xzOut.close();
    in.close();
    System.out.println("File xzeed");

}catch (Exception e){
    System.out.println(e);
}

    }
}
